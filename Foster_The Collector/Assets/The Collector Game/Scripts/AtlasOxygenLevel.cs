using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class AtlasOxygenLevel : MonoBehaviour
{
    private int oxygenLevelAmount = 100;

    private float currentOxygenLevel;

    public Slider oxygenBar;

    public int damage = 10;
    // Start is called before the first frame update
    void Start()
    {
        currentOxygenLevel = oxygenLevelAmount;
        
    }

    // Update is called once per frame
    /*void Update()
    {
        if(currentOxygenLevel <= 0)
        {
            Destroy(gameObject);
        }
    }*/

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Enemy"))
        { 
            currentOxygenLevel = currentOxygenLevel - damage;
            oxygenBar.value = currentOxygenLevel;
            
        }
    }

    public void Hit()
    {
        

        if(currentOxygenLevel == 0)
        {
            Destroy(gameObject);
            SceneManager.LoadScene("Game Over Scene");
        }
    }


}
