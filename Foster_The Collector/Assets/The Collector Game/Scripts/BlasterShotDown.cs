using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlasterShotDown : MonoBehaviour
{
    private float blasterLifeSpan = 5.5f;

    public float speed = 20f;

    private Rigidbody2D projectile;

    //private float damage = 5f;



    private void Awake()
    {
        projectile = GetComponent<Rigidbody2D>();
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        blasterLifeSpan -= Time.deltaTime;

        if (blasterLifeSpan < 0)
        {
            Destroy(gameObject, blasterLifeSpan);
        }

        projectile.velocity = transform.up * speed;
    }

    private void FixedUpdate()
    {
        projectile.AddForce(transform.forward * speed);
    }
}
