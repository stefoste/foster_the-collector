using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RockPickup : MonoBehaviour
{
    private int numberOfRocks = 0;
    public Text rockCountText;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Rock"))
        {
            Destroy(collision.gameObject);
            numberOfRocks++;
            if(rockCountText != null)
            {
                rockCountText.text = "Number of Rocks: " + numberOfRocks.ToString();
            }
            //Debug.Log("Rock Count: " + numberOfRocks);
            
        }


        //Double Rock Value
        if(collision.gameObject.CompareTag("Special Rock"))
        {
            Destroy(collision.gameObject);
            numberOfRocks = numberOfRocks + 4;
            if (rockCountText != null)
            {
                rockCountText.text = "Number of Rocks: " + numberOfRocks.ToString();
            }
        }
    }

    
    // Start is called before the first frame update
    void Start()
    {
        rockCountText.text = "Number of Rocks: " + numberOfRocks.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
