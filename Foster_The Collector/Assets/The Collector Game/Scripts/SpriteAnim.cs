using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteAnim : MonoBehaviour
{
	public Sprite[] poses;
	public Vector2[] toolOffsets;

	public float animSpeed = 12;
	public Sprite Frame(float time)
	{
		if (poses == null || poses.Length == 0) { return null; }
		int frame = ((int)time) % poses.Length;
		return poses[frame];
	}
	public Vector2 OffsetForFrame(float time)
	{
		if (poses == null || poses.Length == 0) { return Vector2.zero; }
		int frame = ((int)time) % toolOffsets.Length;
		return toolOffsets[frame];
	}


	// Start is called before the first frame update
	void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
