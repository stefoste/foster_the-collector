using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shrink : MonoBehaviour
{
    public Vector3 atlasOrginalScale;
    
    // Start is called before the first frame update
    void Start()
    {
        transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y, 1.0f);
        atlasOrginalScale = transform.localScale;
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKey("down"))
        {
            transform.localScale = new Vector3(1.3f, 1.3f, 1.0f);
        }

        if(Input.GetKey("up"))
        {
            transform.localScale = atlasOrginalScale;
        }
    }
}
