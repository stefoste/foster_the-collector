using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TuretMoonBase : MonoBehaviour
{
    public Rigidbody2D blasterProjectile;



    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("ShootBlaster", 1.5f, 5.3f);
    }

    // Update is called once per frame
    void ShootBlaster()
    {
        Rigidbody2D instance = Instantiate(blasterProjectile, transform.position, Quaternion.identity);

        //instance.velocity = transform.TransformDirection(Vector2.right);

        instance.transform.rotation = transform.rotation.normalized;




    }
}
