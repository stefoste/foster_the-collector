using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SideScrollController : CharacterController2D
{
    public Animator atlas_jump;
    
    public float skinWidth = .01f / 16f;

    public float jumpPower = 40f;

    private float terminalVelocity = 40f;

    private float speed = 16.3f;

    public bool jump;

    //public float increasedJumpPower = 40f;

    public bool IsGrounded
    {
        get
        {
            return IsTouching(Vector3.down * skinWidth);
        }
    }
    
    // Start is called before the first frame update
    void Start()
    {
        atlas_jump = gameObject.GetComponent<Animator>();
        jump = false;
    }

    private void FixedUpdate()
    {
        Rigidbody2D player = transform.GetComponent<Rigidbody2D>();
        
        //Prevent Lurching when frames takes too long to load
        if (Time.deltaTime > .1)
        {
            return;
        }

        Vector3 input = Vector3.zero;

        if (Input.GetKey("left"))
        {
            input.x -= 1;
        }

        if(Input.GetKey("right"))
        {
            input.x += 1;
        }


        //Save this for Milestone 3

        

        if (jump == false)
        {
            atlas_jump.SetBool("isJumping", false);
        }

        if(jump == true)
        {
            atlas_jump.SetBool("isJumping", true);
        }

        if (IsGrounded)
        {
            jump = false;
        }


        if(Input.GetKey("j"))
        {
            player.AddForce(new Vector2(0f, Vector2.up.y * jumpPower));
            jump = true;
        }

        /*if(Input.GetKey("h"))
        {
            player.AddForce(new Vector2(0f, Vector2.up.y * increasedJumpPower));
            jump = true;
        }*/

        if(Input.GetKey("d"))
        {
            jump = false;
        }

        /*if(player.velocity.y > -terminalVelocity)
        {
            jumpPower -= terminalVelocity;
        }

        if(player.velocity.y < terminalVelocity)
        {
            jumpPower += terminalVelocity;
        }*/

        //float horizonatalMovement = Input.GetAxis("Horizontal");

        //Vector3 movement = new Vector3(horizonatalMovement, 0, 0);

        Vector3 movement = Vector3.zero;
        
        movement += (input * speed);

        //transform.Translate(movement * speed * Time.deltaTime, Space.World);

        /*if(movement != Vector3.zero)
        {
            transform.forward = movement;
        }*/

        Move(movement * Time.deltaTime);
    }
}
