using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RespawnTriigerMoonBaseTwo : MonoBehaviour
{
    [SerializeField] private Transform player;
    [SerializeField] private Transform respawnCheckpoint;


    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Instant Death"))
        {
            player.position = respawnCheckpoint.position;
            SceneManager.LoadScene("Moon Base Level 2");
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
