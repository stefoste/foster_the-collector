using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingEnemyRovers : MonoBehaviour
{
    private bool dirRight = true;
    private bool dirLeft = true;

    public float speed = 8.5f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
       if(dirRight)
        {
            transform.Translate(Vector2.right * speed * Time.deltaTime);
        } 
       

       /*if(dirLeft)
        {
            transform.Translate(-Vector2.right * speed * Time.deltaTime);
        }*/

       if(transform.position.x >= -14.0f)
        {
            dirRight = true;
            //dirLeft = false;
        }

       if(transform.position.x > 9.0f)
        {
            dirRight = false;
            //dirLeft = true;
        }
    }
}
