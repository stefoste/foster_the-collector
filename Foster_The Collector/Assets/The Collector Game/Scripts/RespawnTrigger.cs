using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class RespawnTrigger : MonoBehaviour
{

    [SerializeField] private Transform player;
    [SerializeField] private Transform respawnCheckpoint;

    //public GameObject respawnCheckpoint;

    //public Vector3 respawnCheckpoint;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.CompareTag("Instant Death"))
        {
            //Debug.Log("You Suck");
            
            player.position = respawnCheckpoint.position;
            //oxygenBar.value = 100;
            SceneManager.LoadScene("Level 1");
        }
        
        /*if(collider.gameObject.CompareTag("Instant Death"))
        {
            this.transform.position = respawnCheckpoint.transform.position;
        }*/
        
        /*if(collider.tag == "Player")
        {
            collider.transform.position = respawnCheckpoint;
        }*/
    }
    // Start is called before the first frame update
    void Start()
    {
        //oxygenBar.value = 0;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
