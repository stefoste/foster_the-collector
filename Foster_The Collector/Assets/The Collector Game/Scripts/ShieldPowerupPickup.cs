using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShieldPowerupPickup : MonoBehaviour
{
    //private int shieldPowerups = 0;
    public Image shieldPowerup;
    //private Coroutine shieldBehavior;
    private bool startedShield;
    //private IEnumerator shield;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Shield"))
        {
            shieldPowerup.enabled = true;
            //shieldPowerups++;
            Destroy(collision.gameObject);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        shieldPowerup.enabled = false;
        startedShield = false;
        //AtlasOxygenLevel atlas = GetComponent<AtlasOxygenLevel>();
        //atlas.damage = 10;
    }

    // Update is called once per frame
    private void FixedUpdate()
    {
        if (Input.GetKey("s"))
        {
            //shieldPowerups--;
            //yield return new WaitForSeconds(10f);
            /*if (!startedShield || shieldPowerups > 0)
            {
                shieldBehavior = StartCoroutine("TemporaryShield");
                startedShield = true;
            }*/
            /*if(shieldPowerups == 0)
            {
                startedShield = false;
                StopAllCoroutines();
                //shieldPowerup.enabled = false;
            }*/
            startedShield = false;
            StopCoroutine(TemporaryShield());
            shieldPowerup.enabled = false;
        }

        if (!startedShield)
        {
            StartCoroutine(TemporaryShield());
            startedShield = true;
        }
    }

    private IEnumerator TemporaryShield()
    {
        AtlasOxygenLevel atlas = GetComponent<AtlasOxygenLevel>();
        atlas.damage = 0;
        //damage = 0;
        //shieldPowerup.color = Color.red;
        //atlas.damage = 10;
        yield return new WaitForSeconds(5f);
        atlas.damage = 10;
        //damage = 10
    }
}
