using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RespawnTriggerNextLevel : MonoBehaviour
{
    [SerializeField] private Transform player;
    [SerializeField] private Transform respawnCheckpoint;


    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Instant Death"))
        {
            //Debug.Log("You Suck");

            player.position = respawnCheckpoint.position;
            //oxygenBar.value = 100;
            SceneManager.LoadScene("Level 2");
        }
    }

    void Start()
    {
        
    }

    void Update()
    {
        
    }

}