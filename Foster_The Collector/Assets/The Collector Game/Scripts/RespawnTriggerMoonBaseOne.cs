using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RespawnTriggerMoonBaseOne : MonoBehaviour
{
    [SerializeField] private Transform player;
    [SerializeField] private Transform respawnCheckpoint;


    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Instant Death"))
        {
            //Debug.Log("You Suck");

            player.position = respawnCheckpoint.position;
            //oxygenBar.value = 100;
            SceneManager.LoadScene("Moon Base Level 1");
        }
    }

    void Start()
    {

    }

    void Update()
    {

    }
}
